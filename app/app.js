import express from 'express';

const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const { Users } = require('./database');
const { ApolloServer } = require('apollo-server-express')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

const server = new ApolloServer({
    modules: [
        require('./graphql/users.graphql'),
        require('./graphql/addresses.graphql'),
        require('./graphql/phones.graphql'),
    ],
})

server.applyMiddleware({ app });

app.get('/', (req, res) => res.send('Hello Worlds!'));

app.get('/api/users', (req, res) => {
    Users.findAll().then(users => res.json(users));
})

app.listen({ port: 3000 }, () =>
    console.log(`🚀 Server readys at http://localhost:3000`),
)