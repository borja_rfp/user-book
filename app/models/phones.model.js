module.exports = (sequelize, type) => {
    return sequelize.define('phones', {
        id: {
            type: type.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true

        },
        user_id: {
            type: type.INTEGER,
            allowNull: false
        },
        phone: {
            type: type.STRING(256),
            allowNull: false
        },
        country_code: {
            type: type.STRING(256),
            allowNull: false
        },
    }, 
    {
        timestamps: false,
    });
}