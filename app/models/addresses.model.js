module.exports = (sequelize, type) => {
    return sequelize.define('addresses', {
        id: {
            type: type.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true

        },
        user_id: {
            type: type.INTEGER,
            allowNull: false
        },
        address: {
            type: type.STRING(256),
            allowNull: false
        },
        zp: {
            type: type.STRING(5),
            allowNull: false
        },
        city: {
            type: type.STRING(256),
            allowNull: false
        },
        country: {
            type: type.STRING(256),
            allowNull: false
        },
    }, 
    {
        timestamps: false,
    });
}