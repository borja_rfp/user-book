module.exports = (sequelize, type) => {
    return sequelize.define('users', {
        id: {
            type: type.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true

        },
        name: {
            type: type.STRING(256),
            allowNull: false
        },
        first_name: {
            type: type.STRING(256),
            allowNull: false
        },
        email: {
            type: type.STRING(256),
            allowNull: false
        },
    }, 
    {
        timestamps: false,
    });
}