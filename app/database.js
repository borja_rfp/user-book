const Sequelize = require('sequelize');
const UsersModel = require('./models/users.model');
const AddressesModel = require('./models/addresses.model');
const PhonesModel = require('./models/phones.model');

const sequelize = new Sequelize('userbook', 'root', '123456', {
    host: 'localhost',
    port: '8006',
    dialect: 'mysql',
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

const Users = UsersModel(sequelize, Sequelize);
const Addresses = AddressesModel(sequelize, Sequelize);
const Phones = PhonesModel(sequelize, Sequelize);

module.exports = {
    Users,
    Addresses,
    Phones,
    Sequelize,
};

