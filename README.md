# User Book application

User Book is an application which is an example for learn GraphQL.

## Installation

For execute the application you need install in your computer the last version of NodeJS and Yarn, then:

```bash
yarn install
```

## Usage

```bash
yarn start
```
Generate a server and is watching all the changes. Execute in your browser http://localhost:3000