/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/app.js":
/*!********************!*\
  !*** ./app/app.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);


const bodyParser = __webpack_require__(/*! body-parser */ "body-parser");
const cors = __webpack_require__(/*! cors */ "cors");
const app = express__WEBPACK_IMPORTED_MODULE_0___default()();
const { Users } = __webpack_require__(/*! ./database */ "./app/database.js");
const { ApolloServer } = __webpack_require__(/*! apollo-server-express */ "apollo-server-express")
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

const server = new ApolloServer({
    modules: [
        __webpack_require__(/*! ./graphql/users.graphql */ "./app/graphql/users.graphql.js"),
        __webpack_require__(/*! ./graphql/addresses.graphql */ "./app/graphql/addresses.graphql.js"),
        __webpack_require__(/*! ./graphql/phones.graphql */ "./app/graphql/phones.graphql.js"),
    ],
})

server.applyMiddleware({ app });

app.get('/', (req, res) => res.send('Hello Worlds!'));

app.get('/api/users', (req, res) => {
    Users.findAll().then(users => res.json(users));
})

app.listen({ port: 3000 }, () =>
    console.log(`🚀 Server readys at http://localhost:3000`),
)

/***/ }),

/***/ "./app/database.js":
/*!*************************!*\
  !*** ./app/database.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const Sequelize = __webpack_require__(/*! sequelize */ "sequelize");
const UsersModel = __webpack_require__(/*! ./models/users.model */ "./app/models/users.model.js");
const AddressesModel = __webpack_require__(/*! ./models/addresses.model */ "./app/models/addresses.model.js");
const PhonesModel = __webpack_require__(/*! ./models/phones.model */ "./app/models/phones.model.js");

const sequelize = new Sequelize('userbook', 'root', '123456', {
    host: 'localhost',
    port: '8006',
    dialect: 'mysql',
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

const Users = UsersModel(sequelize, Sequelize);
const Addresses = AddressesModel(sequelize, Sequelize);
const Phones = PhonesModel(sequelize, Sequelize);

module.exports = {
    Users,
    Addresses,
    Phones,
    Sequelize,
};



/***/ }),

/***/ "./app/graphql/addresses.graphql.js":
/*!******************************************!*\
  !*** ./app/graphql/addresses.graphql.js ***!
  \******************************************/
/*! exports provided: typeDefs, resolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "typeDefs", function() { return typeDefs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolvers", function() { return resolvers; });
/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! apollo-server-express */ "apollo-server-express");
/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(apollo_server_express__WEBPACK_IMPORTED_MODULE_0__);

const { Addresses } = __webpack_require__(/*! ../database */ "./app/database.js");

const typeDefs = apollo_server_express__WEBPACK_IMPORTED_MODULE_0__["gql"]`

    type Address {
        id: ID!
        address: String!
        zp: String
        city: String
        country: String
    }

    extend type Query {
        addresses: [Address]!
        address(id: ID!): Address
        countAddress: Int!
    }
`;

const resolvers = {
    Query: {
        addresses: async () => Addresses.findAll(),
        address: async(obj, args, contet, info) => Addresses.findByPk(args.id),
        countAddress:  () => { 
            return Addresses.findAndCountAll().then(obj => obj.count);
        }
    },
}

/***/ }),

/***/ "./app/graphql/phones.graphql.js":
/*!***************************************!*\
  !*** ./app/graphql/phones.graphql.js ***!
  \***************************************/
/*! exports provided: typeDefs, resolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "typeDefs", function() { return typeDefs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolvers", function() { return resolvers; });
/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! apollo-server-express */ "apollo-server-express");
/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(apollo_server_express__WEBPACK_IMPORTED_MODULE_0__);

const { Phones } = __webpack_require__(/*! ../database */ "./app/database.js");

const typeDefs = apollo_server_express__WEBPACK_IMPORTED_MODULE_0__["gql"]`

    type Phone {
        id: ID!
        phone: String!
        country_code: String
    }

    extend type Query {
        phones: [Phone]!
        phone(id: ID!): Phone
        countPhones: Int!
    }
`;

const resolvers = {
    Query: {
        phones: async () => Phones.findAll(),
        phone: async(obj, args, contet, info) => Phones.findByPk(args.id),
        countPhones:  () => { 
            return Phones.findAndCountAll().then(obj => obj.count);
        }
    },
}

/***/ }),

/***/ "./app/graphql/users.graphql.js":
/*!**************************************!*\
  !*** ./app/graphql/users.graphql.js ***!
  \**************************************/
/*! exports provided: typeDefs, resolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "typeDefs", function() { return typeDefs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolvers", function() { return resolvers; });
/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! apollo-server-express */ "apollo-server-express");
/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(apollo_server_express__WEBPACK_IMPORTED_MODULE_0__);

const { Users, Addresses, Phones } = __webpack_require__(/*! ../database */ "./app/database.js");

const typeDefs = apollo_server_express__WEBPACK_IMPORTED_MODULE_0__["gql"]`

    type User {
        id: ID!
        name: String!
        first_name: String
        email: String!
        address: Address
        phone: Phone
    }

    input CreateUserInput {
        name: String!
        first_name: String
        email: String!
    }

    input CreatePhoneInput {
        phone: String!
        country_code: String
    }

    input CreateAddressInput {
        address: String!
        zp: String
        city: String
        country: String
    }

    extend type Query {
        users: [User]!
        user(id: ID!): User
        countUsers: Int!
    }

    extend type Mutation {
        createUser(createUserInput: CreateUserInput!, createAddressInput: CreateAddressInput!, createPhoneInput: CreatePhoneInput!): User!
        deleteUser(id: ID!): Boolean
    }
`;

const resolvers = {
    Query: {
        users: async () => Users.findAll(),
        user: async(obj, args, contet, info) => Users.findByPk(args.id),
        countUsers:  () => { 
            return Users.findAndCountAll().then(obj => obj.count);
        }
    },
    User: {
        address: async (obj, args, context, info) => Addresses.findOne({ where: { user_id: obj.id } }),
        phone: async (obj, args, context, info) => Phones.findOne({ where: { user_id: obj.id } }),
    },
    Mutation: {
        createUser: async (parent, args) => {
            const createUser = await Users.create({
                name: args.createUserInput.name,
                first_name: args.createUserInput.first_name,
                email: args.createUserInput.email,
            });
            const userId = createUser.dataValues.id;
            const createAddress = await Addresses.create({
                user_id: userId,
                address: args.createAddressInput.address,
                zp: args.createAddressInput.zp,
                city: args.createAddressInput.city,
                country: args.createAddressInput.country,
            });
            const createPhone = await Phones.create({
                user_id: userId,
                phone: args.createPhoneInput.phone,
                country_code: args.createPhoneInput.country_code,
            });
            createUser.address = createAddress.dataValues;
            createUser.phone = createPhone.dataValues;
            return createUser.dataValues;
        },

        deleteUser: async (parent, args) => {
            await Users.destroy({
                where: {
                    id: args.id,
                },
            });
            await Addresses.destroy({
                where: {
                    user_id: args.id,
                },
            });
            await Phones.destroy({
                where: {
                    user_id: args.id,
                },
            });

            return true;
        }
    }
    
}

/***/ }),

/***/ "./app/models/addresses.model.js":
/*!***************************************!*\
  !*** ./app/models/addresses.model.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = (sequelize, type) => {
    return sequelize.define('addresses', {
        id: {
            type: type.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true

        },
        user_id: {
            type: type.INTEGER,
            allowNull: false
        },
        address: {
            type: type.STRING(256),
            allowNull: false
        },
        zp: {
            type: type.STRING(5),
            allowNull: false
        },
        city: {
            type: type.STRING(256),
            allowNull: false
        },
        country: {
            type: type.STRING(256),
            allowNull: false
        },
    }, 
    {
        timestamps: false,
    });
}

/***/ }),

/***/ "./app/models/phones.model.js":
/*!************************************!*\
  !*** ./app/models/phones.model.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = (sequelize, type) => {
    return sequelize.define('phones', {
        id: {
            type: type.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true

        },
        user_id: {
            type: type.INTEGER,
            allowNull: false
        },
        phone: {
            type: type.STRING(256),
            allowNull: false
        },
        country_code: {
            type: type.STRING(256),
            allowNull: false
        },
    }, 
    {
        timestamps: false,
    });
}

/***/ }),

/***/ "./app/models/users.model.js":
/*!***********************************!*\
  !*** ./app/models/users.model.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = (sequelize, type) => {
    return sequelize.define('users', {
        id: {
            type: type.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true

        },
        name: {
            type: type.STRING(256),
            allowNull: false
        },
        first_name: {
            type: type.STRING(256),
            allowNull: false
        },
        email: {
            type: type.STRING(256),
            allowNull: false
        },
    }, 
    {
        timestamps: false,
    });
}

/***/ }),

/***/ "apollo-server-express":
/*!****************************************!*\
  !*** external "apollo-server-express" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("apollo-server-express");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "sequelize":
/*!****************************!*\
  !*** external "sequelize" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sequelize");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXBwL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvZGF0YWJhc2UuanMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2dyYXBocWwvYWRkcmVzc2VzLmdyYXBocWwuanMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2dyYXBocWwvcGhvbmVzLmdyYXBocWwuanMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2dyYXBocWwvdXNlcnMuZ3JhcGhxbC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvbW9kZWxzL2FkZHJlc3Nlcy5tb2RlbC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvbW9kZWxzL3Bob25lcy5tb2RlbC5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvbW9kZWxzL3VzZXJzLm1vZGVsLmpzIiwid2VicGFjazovLy9leHRlcm5hbCBcImFwb2xsby1zZXJ2ZXItZXhwcmVzc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImJvZHktcGFyc2VyXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiY29yc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImV4cHJlc3NcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJzZXF1ZWxpemVcIiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUE4Qjs7QUFFOUIsbUJBQW1CLG1CQUFPLENBQUMsZ0NBQWE7QUFDeEMsYUFBYSxtQkFBTyxDQUFDLGtCQUFNO0FBQzNCLFlBQVksOENBQU87QUFDbkIsT0FBTyxRQUFRLEdBQUcsbUJBQU8sQ0FBQyxxQ0FBWTtBQUN0QyxPQUFPLGVBQWUsR0FBRyxtQkFBTyxDQUFDLG9EQUF1QjtBQUN4RDtBQUNBLCtCQUErQixpQkFBaUI7QUFDaEQ7O0FBRUE7QUFDQTtBQUNBLFFBQVEsbUJBQU8sQ0FBQywrREFBeUI7QUFDekMsUUFBUSxtQkFBTyxDQUFDLHVFQUE2QjtBQUM3QyxRQUFRLG1CQUFPLENBQUMsaUVBQTBCO0FBQzFDO0FBQ0EsQ0FBQzs7QUFFRCx3QkFBd0IsTUFBTTs7QUFFOUI7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQsWUFBWSxhQUFhO0FBQ3pCO0FBQ0EsQzs7Ozs7Ozs7Ozs7QUM3QkEsa0JBQWtCLG1CQUFPLENBQUMsNEJBQVc7QUFDckMsbUJBQW1CLG1CQUFPLENBQUMseURBQXNCO0FBQ2pELHVCQUF1QixtQkFBTyxDQUFDLGlFQUEwQjtBQUN6RCxvQkFBb0IsbUJBQU8sQ0FBQywyREFBdUI7O0FBRW5EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzFCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTRDO0FBQzVDLE9BQU8sWUFBWSxHQUFHLG1CQUFPLENBQUMsc0NBQWE7O0FBRXBDLGlCQUFpQix5REFBRzs7QUFFM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsOEI7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEM7Ozs7Ozs7Ozs7OztBQzVCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTRDO0FBQzVDLE9BQU8sU0FBUyxHQUFHLG1CQUFPLENBQUMsc0NBQWE7O0FBRWpDLGlCQUFpQix5REFBRzs7QUFFM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRU87QUFDUDtBQUNBO0FBQ0E7QUFDQSw2QjtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7Ozs7O0FDMUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBNEM7QUFDNUMsT0FBTywyQkFBMkIsR0FBRyxtQkFBTyxDQUFDLHNDQUFhOztBQUVuRCxpQkFBaUIseURBQUc7O0FBRTNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsNEI7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0Esd0VBQXdFLFNBQVMsa0JBQWtCLEVBQUU7QUFDckcsbUVBQW1FLFNBQVMsa0JBQWtCLEVBQUU7QUFDaEcsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhOztBQUViO0FBQ0E7QUFDQTs7QUFFQSxDOzs7Ozs7Ozs7OztBQ3RHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTCxDOzs7Ozs7Ozs7OztBQ2pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7Ozs7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMLEM7Ozs7Ozs7Ozs7O0FDekJBLGtEOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLGlDOzs7Ozs7Ozs7OztBQ0FBLG9DOzs7Ozs7Ozs7OztBQ0FBLHNDIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vYXBwL2FwcC5qc1wiKTtcbiIsImltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xuXG5jb25zdCBib2R5UGFyc2VyID0gcmVxdWlyZSgnYm9keS1wYXJzZXInKTtcbmNvbnN0IGNvcnMgPSByZXF1aXJlKCdjb3JzJyk7XG5jb25zdCBhcHAgPSBleHByZXNzKCk7XG5jb25zdCB7IFVzZXJzIH0gPSByZXF1aXJlKCcuL2RhdGFiYXNlJyk7XG5jb25zdCB7IEFwb2xsb1NlcnZlciB9ID0gcmVxdWlyZSgnYXBvbGxvLXNlcnZlci1leHByZXNzJylcbmFwcC51c2UoYm9keVBhcnNlci5qc29uKCkpO1xuYXBwLnVzZShib2R5UGFyc2VyLnVybGVuY29kZWQoeyBleHRlbmRlZDogdHJ1ZSB9KSk7XG5hcHAudXNlKGNvcnMoKSk7XG5cbmNvbnN0IHNlcnZlciA9IG5ldyBBcG9sbG9TZXJ2ZXIoe1xuICAgIG1vZHVsZXM6IFtcbiAgICAgICAgcmVxdWlyZSgnLi9ncmFwaHFsL3VzZXJzLmdyYXBocWwnKSxcbiAgICAgICAgcmVxdWlyZSgnLi9ncmFwaHFsL2FkZHJlc3Nlcy5ncmFwaHFsJyksXG4gICAgICAgIHJlcXVpcmUoJy4vZ3JhcGhxbC9waG9uZXMuZ3JhcGhxbCcpLFxuICAgIF0sXG59KVxuXG5zZXJ2ZXIuYXBwbHlNaWRkbGV3YXJlKHsgYXBwIH0pO1xuXG5hcHAuZ2V0KCcvJywgKHJlcSwgcmVzKSA9PiByZXMuc2VuZCgnSGVsbG8gV29ybGRzIScpKTtcblxuYXBwLmdldCgnL2FwaS91c2VycycsIChyZXEsIHJlcykgPT4ge1xuICAgIFVzZXJzLmZpbmRBbGwoKS50aGVuKHVzZXJzID0+IHJlcy5qc29uKHVzZXJzKSk7XG59KVxuXG5hcHAubGlzdGVuKHsgcG9ydDogMzAwMCB9LCAoKSA9PlxuICAgIGNvbnNvbGUubG9nKGDwn5qAIFNlcnZlciByZWFkeXMgYXQgaHR0cDovL2xvY2FsaG9zdDozMDAwYCksXG4pIiwiY29uc3QgU2VxdWVsaXplID0gcmVxdWlyZSgnc2VxdWVsaXplJyk7XG5jb25zdCBVc2Vyc01vZGVsID0gcmVxdWlyZSgnLi9tb2RlbHMvdXNlcnMubW9kZWwnKTtcbmNvbnN0IEFkZHJlc3Nlc01vZGVsID0gcmVxdWlyZSgnLi9tb2RlbHMvYWRkcmVzc2VzLm1vZGVsJyk7XG5jb25zdCBQaG9uZXNNb2RlbCA9IHJlcXVpcmUoJy4vbW9kZWxzL3Bob25lcy5tb2RlbCcpO1xuXG5jb25zdCBzZXF1ZWxpemUgPSBuZXcgU2VxdWVsaXplKCd1c2VyYm9vaycsICdyb290JywgJzEyMzQ1NicsIHtcbiAgICBob3N0OiAnbG9jYWxob3N0JyxcbiAgICBwb3J0OiAnODAwNicsXG4gICAgZGlhbGVjdDogJ215c3FsJyxcbiAgICBwb29sOiB7XG4gICAgICAgIG1heDogMTAsXG4gICAgICAgIG1pbjogMCxcbiAgICAgICAgYWNxdWlyZTogMzAwMDAsXG4gICAgICAgIGlkbGU6IDEwMDAwXG4gICAgfVxufSk7XG5cbmNvbnN0IFVzZXJzID0gVXNlcnNNb2RlbChzZXF1ZWxpemUsIFNlcXVlbGl6ZSk7XG5jb25zdCBBZGRyZXNzZXMgPSBBZGRyZXNzZXNNb2RlbChzZXF1ZWxpemUsIFNlcXVlbGl6ZSk7XG5jb25zdCBQaG9uZXMgPSBQaG9uZXNNb2RlbChzZXF1ZWxpemUsIFNlcXVlbGl6ZSk7XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICAgIFVzZXJzLFxuICAgIEFkZHJlc3NlcyxcbiAgICBQaG9uZXMsXG4gICAgU2VxdWVsaXplLFxufTtcblxuIiwiaW1wb3J0IHsgZ3FsIH0gZnJvbSAnYXBvbGxvLXNlcnZlci1leHByZXNzJztcbmNvbnN0IHsgQWRkcmVzc2VzIH0gPSByZXF1aXJlKCcuLi9kYXRhYmFzZScpO1xuXG5leHBvcnQgY29uc3QgdHlwZURlZnMgPSBncWxgXG5cbiAgICB0eXBlIEFkZHJlc3Mge1xuICAgICAgICBpZDogSUQhXG4gICAgICAgIGFkZHJlc3M6IFN0cmluZyFcbiAgICAgICAgenA6IFN0cmluZ1xuICAgICAgICBjaXR5OiBTdHJpbmdcbiAgICAgICAgY291bnRyeTogU3RyaW5nXG4gICAgfVxuXG4gICAgZXh0ZW5kIHR5cGUgUXVlcnkge1xuICAgICAgICBhZGRyZXNzZXM6IFtBZGRyZXNzXSFcbiAgICAgICAgYWRkcmVzcyhpZDogSUQhKTogQWRkcmVzc1xuICAgICAgICBjb3VudEFkZHJlc3M6IEludCFcbiAgICB9XG5gO1xuXG5leHBvcnQgY29uc3QgcmVzb2x2ZXJzID0ge1xuICAgIFF1ZXJ5OiB7XG4gICAgICAgIGFkZHJlc3NlczogYXN5bmMgKCkgPT4gQWRkcmVzc2VzLmZpbmRBbGwoKSxcbiAgICAgICAgYWRkcmVzczogYXN5bmMob2JqLCBhcmdzLCBjb250ZXQsIGluZm8pID0+IEFkZHJlc3Nlcy5maW5kQnlQayhhcmdzLmlkKSxcbiAgICAgICAgY291bnRBZGRyZXNzOiAgKCkgPT4geyBcbiAgICAgICAgICAgIHJldHVybiBBZGRyZXNzZXMuZmluZEFuZENvdW50QWxsKCkudGhlbihvYmogPT4gb2JqLmNvdW50KTtcbiAgICAgICAgfVxuICAgIH0sXG59IiwiaW1wb3J0IHsgZ3FsIH0gZnJvbSAnYXBvbGxvLXNlcnZlci1leHByZXNzJztcbmNvbnN0IHsgUGhvbmVzIH0gPSByZXF1aXJlKCcuLi9kYXRhYmFzZScpO1xuXG5leHBvcnQgY29uc3QgdHlwZURlZnMgPSBncWxgXG5cbiAgICB0eXBlIFBob25lIHtcbiAgICAgICAgaWQ6IElEIVxuICAgICAgICBwaG9uZTogU3RyaW5nIVxuICAgICAgICBjb3VudHJ5X2NvZGU6IFN0cmluZ1xuICAgIH1cblxuICAgIGV4dGVuZCB0eXBlIFF1ZXJ5IHtcbiAgICAgICAgcGhvbmVzOiBbUGhvbmVdIVxuICAgICAgICBwaG9uZShpZDogSUQhKTogUGhvbmVcbiAgICAgICAgY291bnRQaG9uZXM6IEludCFcbiAgICB9XG5gO1xuXG5leHBvcnQgY29uc3QgcmVzb2x2ZXJzID0ge1xuICAgIFF1ZXJ5OiB7XG4gICAgICAgIHBob25lczogYXN5bmMgKCkgPT4gUGhvbmVzLmZpbmRBbGwoKSxcbiAgICAgICAgcGhvbmU6IGFzeW5jKG9iaiwgYXJncywgY29udGV0LCBpbmZvKSA9PiBQaG9uZXMuZmluZEJ5UGsoYXJncy5pZCksXG4gICAgICAgIGNvdW50UGhvbmVzOiAgKCkgPT4geyBcbiAgICAgICAgICAgIHJldHVybiBQaG9uZXMuZmluZEFuZENvdW50QWxsKCkudGhlbihvYmogPT4gb2JqLmNvdW50KTtcbiAgICAgICAgfVxuICAgIH0sXG59IiwiaW1wb3J0IHsgZ3FsIH0gZnJvbSAnYXBvbGxvLXNlcnZlci1leHByZXNzJztcbmNvbnN0IHsgVXNlcnMsIEFkZHJlc3NlcywgUGhvbmVzIH0gPSByZXF1aXJlKCcuLi9kYXRhYmFzZScpO1xuXG5leHBvcnQgY29uc3QgdHlwZURlZnMgPSBncWxgXG5cbiAgICB0eXBlIFVzZXIge1xuICAgICAgICBpZDogSUQhXG4gICAgICAgIG5hbWU6IFN0cmluZyFcbiAgICAgICAgZmlyc3RfbmFtZTogU3RyaW5nXG4gICAgICAgIGVtYWlsOiBTdHJpbmchXG4gICAgICAgIGFkZHJlc3M6IEFkZHJlc3NcbiAgICAgICAgcGhvbmU6IFBob25lXG4gICAgfVxuXG4gICAgaW5wdXQgQ3JlYXRlVXNlcklucHV0IHtcbiAgICAgICAgbmFtZTogU3RyaW5nIVxuICAgICAgICBmaXJzdF9uYW1lOiBTdHJpbmdcbiAgICAgICAgZW1haWw6IFN0cmluZyFcbiAgICB9XG5cbiAgICBpbnB1dCBDcmVhdGVQaG9uZUlucHV0IHtcbiAgICAgICAgcGhvbmU6IFN0cmluZyFcbiAgICAgICAgY291bnRyeV9jb2RlOiBTdHJpbmdcbiAgICB9XG5cbiAgICBpbnB1dCBDcmVhdGVBZGRyZXNzSW5wdXQge1xuICAgICAgICBhZGRyZXNzOiBTdHJpbmchXG4gICAgICAgIHpwOiBTdHJpbmdcbiAgICAgICAgY2l0eTogU3RyaW5nXG4gICAgICAgIGNvdW50cnk6IFN0cmluZ1xuICAgIH1cblxuICAgIGV4dGVuZCB0eXBlIFF1ZXJ5IHtcbiAgICAgICAgdXNlcnM6IFtVc2VyXSFcbiAgICAgICAgdXNlcihpZDogSUQhKTogVXNlclxuICAgICAgICBjb3VudFVzZXJzOiBJbnQhXG4gICAgfVxuXG4gICAgZXh0ZW5kIHR5cGUgTXV0YXRpb24ge1xuICAgICAgICBjcmVhdGVVc2VyKGNyZWF0ZVVzZXJJbnB1dDogQ3JlYXRlVXNlcklucHV0ISwgY3JlYXRlQWRkcmVzc0lucHV0OiBDcmVhdGVBZGRyZXNzSW5wdXQhLCBjcmVhdGVQaG9uZUlucHV0OiBDcmVhdGVQaG9uZUlucHV0ISk6IFVzZXIhXG4gICAgICAgIGRlbGV0ZVVzZXIoaWQ6IElEISk6IEJvb2xlYW5cbiAgICB9XG5gO1xuXG5leHBvcnQgY29uc3QgcmVzb2x2ZXJzID0ge1xuICAgIFF1ZXJ5OiB7XG4gICAgICAgIHVzZXJzOiBhc3luYyAoKSA9PiBVc2Vycy5maW5kQWxsKCksXG4gICAgICAgIHVzZXI6IGFzeW5jKG9iaiwgYXJncywgY29udGV0LCBpbmZvKSA9PiBVc2Vycy5maW5kQnlQayhhcmdzLmlkKSxcbiAgICAgICAgY291bnRVc2VyczogICgpID0+IHsgXG4gICAgICAgICAgICByZXR1cm4gVXNlcnMuZmluZEFuZENvdW50QWxsKCkudGhlbihvYmogPT4gb2JqLmNvdW50KTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgVXNlcjoge1xuICAgICAgICBhZGRyZXNzOiBhc3luYyAob2JqLCBhcmdzLCBjb250ZXh0LCBpbmZvKSA9PiBBZGRyZXNzZXMuZmluZE9uZSh7IHdoZXJlOiB7IHVzZXJfaWQ6IG9iai5pZCB9IH0pLFxuICAgICAgICBwaG9uZTogYXN5bmMgKG9iaiwgYXJncywgY29udGV4dCwgaW5mbykgPT4gUGhvbmVzLmZpbmRPbmUoeyB3aGVyZTogeyB1c2VyX2lkOiBvYmouaWQgfSB9KSxcbiAgICB9LFxuICAgIE11dGF0aW9uOiB7XG4gICAgICAgIGNyZWF0ZVVzZXI6IGFzeW5jIChwYXJlbnQsIGFyZ3MpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGNyZWF0ZVVzZXIgPSBhd2FpdCBVc2Vycy5jcmVhdGUoe1xuICAgICAgICAgICAgICAgIG5hbWU6IGFyZ3MuY3JlYXRlVXNlcklucHV0Lm5hbWUsXG4gICAgICAgICAgICAgICAgZmlyc3RfbmFtZTogYXJncy5jcmVhdGVVc2VySW5wdXQuZmlyc3RfbmFtZSxcbiAgICAgICAgICAgICAgICBlbWFpbDogYXJncy5jcmVhdGVVc2VySW5wdXQuZW1haWwsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGNvbnN0IHVzZXJJZCA9IGNyZWF0ZVVzZXIuZGF0YVZhbHVlcy5pZDtcbiAgICAgICAgICAgIGNvbnN0IGNyZWF0ZUFkZHJlc3MgPSBhd2FpdCBBZGRyZXNzZXMuY3JlYXRlKHtcbiAgICAgICAgICAgICAgICB1c2VyX2lkOiB1c2VySWQsXG4gICAgICAgICAgICAgICAgYWRkcmVzczogYXJncy5jcmVhdGVBZGRyZXNzSW5wdXQuYWRkcmVzcyxcbiAgICAgICAgICAgICAgICB6cDogYXJncy5jcmVhdGVBZGRyZXNzSW5wdXQuenAsXG4gICAgICAgICAgICAgICAgY2l0eTogYXJncy5jcmVhdGVBZGRyZXNzSW5wdXQuY2l0eSxcbiAgICAgICAgICAgICAgICBjb3VudHJ5OiBhcmdzLmNyZWF0ZUFkZHJlc3NJbnB1dC5jb3VudHJ5LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBjcmVhdGVQaG9uZSA9IGF3YWl0IFBob25lcy5jcmVhdGUoe1xuICAgICAgICAgICAgICAgIHVzZXJfaWQ6IHVzZXJJZCxcbiAgICAgICAgICAgICAgICBwaG9uZTogYXJncy5jcmVhdGVQaG9uZUlucHV0LnBob25lLFxuICAgICAgICAgICAgICAgIGNvdW50cnlfY29kZTogYXJncy5jcmVhdGVQaG9uZUlucHV0LmNvdW50cnlfY29kZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY3JlYXRlVXNlci5hZGRyZXNzID0gY3JlYXRlQWRkcmVzcy5kYXRhVmFsdWVzO1xuICAgICAgICAgICAgY3JlYXRlVXNlci5waG9uZSA9IGNyZWF0ZVBob25lLmRhdGFWYWx1ZXM7XG4gICAgICAgICAgICByZXR1cm4gY3JlYXRlVXNlci5kYXRhVmFsdWVzO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlbGV0ZVVzZXI6IGFzeW5jIChwYXJlbnQsIGFyZ3MpID0+IHtcbiAgICAgICAgICAgIGF3YWl0IFVzZXJzLmRlc3Ryb3koe1xuICAgICAgICAgICAgICAgIHdoZXJlOiB7XG4gICAgICAgICAgICAgICAgICAgIGlkOiBhcmdzLmlkLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGF3YWl0IEFkZHJlc3Nlcy5kZXN0cm95KHtcbiAgICAgICAgICAgICAgICB3aGVyZToge1xuICAgICAgICAgICAgICAgICAgICB1c2VyX2lkOiBhcmdzLmlkLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGF3YWl0IFBob25lcy5kZXN0cm95KHtcbiAgICAgICAgICAgICAgICB3aGVyZToge1xuICAgICAgICAgICAgICAgICAgICB1c2VyX2lkOiBhcmdzLmlkLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICB9XG4gICAgXG59IiwibW9kdWxlLmV4cG9ydHMgPSAoc2VxdWVsaXplLCB0eXBlKSA9PiB7XG4gICAgcmV0dXJuIHNlcXVlbGl6ZS5kZWZpbmUoJ2FkZHJlc3NlcycsIHtcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuSU5URUdFUixcbiAgICAgICAgICAgIGFsbG93TnVsbDogZmFsc2UsXG4gICAgICAgICAgICBwcmltYXJ5S2V5OiB0cnVlLFxuICAgICAgICAgICAgYXV0b0luY3JlbWVudDogdHJ1ZVxuXG4gICAgICAgIH0sXG4gICAgICAgIHVzZXJfaWQ6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuSU5URUdFUixcbiAgICAgICAgICAgIGFsbG93TnVsbDogZmFsc2VcbiAgICAgICAgfSxcbiAgICAgICAgYWRkcmVzczoge1xuICAgICAgICAgICAgdHlwZTogdHlwZS5TVFJJTkcoMjU2KSxcbiAgICAgICAgICAgIGFsbG93TnVsbDogZmFsc2VcbiAgICAgICAgfSxcbiAgICAgICAgenA6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuU1RSSU5HKDUpLFxuICAgICAgICAgICAgYWxsb3dOdWxsOiBmYWxzZVxuICAgICAgICB9LFxuICAgICAgICBjaXR5OiB7XG4gICAgICAgICAgICB0eXBlOiB0eXBlLlNUUklORygyNTYpLFxuICAgICAgICAgICAgYWxsb3dOdWxsOiBmYWxzZVxuICAgICAgICB9LFxuICAgICAgICBjb3VudHJ5OiB7XG4gICAgICAgICAgICB0eXBlOiB0eXBlLlNUUklORygyNTYpLFxuICAgICAgICAgICAgYWxsb3dOdWxsOiBmYWxzZVxuICAgICAgICB9LFxuICAgIH0sIFxuICAgIHtcbiAgICAgICAgdGltZXN0YW1wczogZmFsc2UsXG4gICAgfSk7XG59IiwibW9kdWxlLmV4cG9ydHMgPSAoc2VxdWVsaXplLCB0eXBlKSA9PiB7XG4gICAgcmV0dXJuIHNlcXVlbGl6ZS5kZWZpbmUoJ3Bob25lcycsIHtcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuSU5URUdFUixcbiAgICAgICAgICAgIGFsbG93TnVsbDogZmFsc2UsXG4gICAgICAgICAgICBwcmltYXJ5S2V5OiB0cnVlLFxuICAgICAgICAgICAgYXV0b0luY3JlbWVudDogdHJ1ZVxuXG4gICAgICAgIH0sXG4gICAgICAgIHVzZXJfaWQ6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuSU5URUdFUixcbiAgICAgICAgICAgIGFsbG93TnVsbDogZmFsc2VcbiAgICAgICAgfSxcbiAgICAgICAgcGhvbmU6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuU1RSSU5HKDI1NiksXG4gICAgICAgICAgICBhbGxvd051bGw6IGZhbHNlXG4gICAgICAgIH0sXG4gICAgICAgIGNvdW50cnlfY29kZToge1xuICAgICAgICAgICAgdHlwZTogdHlwZS5TVFJJTkcoMjU2KSxcbiAgICAgICAgICAgIGFsbG93TnVsbDogZmFsc2VcbiAgICAgICAgfSxcbiAgICB9LCBcbiAgICB7XG4gICAgICAgIHRpbWVzdGFtcHM6IGZhbHNlLFxuICAgIH0pO1xufSIsIm1vZHVsZS5leHBvcnRzID0gKHNlcXVlbGl6ZSwgdHlwZSkgPT4ge1xuICAgIHJldHVybiBzZXF1ZWxpemUuZGVmaW5lKCd1c2VycycsIHtcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuSU5URUdFUixcbiAgICAgICAgICAgIGFsbG93TnVsbDogZmFsc2UsXG4gICAgICAgICAgICBwcmltYXJ5S2V5OiB0cnVlLFxuICAgICAgICAgICAgYXV0b0luY3JlbWVudDogdHJ1ZVxuXG4gICAgICAgIH0sXG4gICAgICAgIG5hbWU6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuU1RSSU5HKDI1NiksXG4gICAgICAgICAgICBhbGxvd051bGw6IGZhbHNlXG4gICAgICAgIH0sXG4gICAgICAgIGZpcnN0X25hbWU6IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUuU1RSSU5HKDI1NiksXG4gICAgICAgICAgICBhbGxvd051bGw6IGZhbHNlXG4gICAgICAgIH0sXG4gICAgICAgIGVtYWlsOiB7XG4gICAgICAgICAgICB0eXBlOiB0eXBlLlNUUklORygyNTYpLFxuICAgICAgICAgICAgYWxsb3dOdWxsOiBmYWxzZVxuICAgICAgICB9LFxuICAgIH0sIFxuICAgIHtcbiAgICAgICAgdGltZXN0YW1wczogZmFsc2UsXG4gICAgfSk7XG59IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYXBvbGxvLXNlcnZlci1leHByZXNzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImJvZHktcGFyc2VyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImNvcnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXhwcmVzc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzZXF1ZWxpemVcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==